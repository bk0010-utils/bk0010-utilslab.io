function run(file, from, optimize) {
	toCanvas(file, (w, h, ctx) => {
		let data = ctx.getImageData(0, 0, w, h).data;

		let res = [];
		let offsets = [];
		let charData = "";
		let allCharData = [];
		let repeat = [];
		let repeatCnt = 0;
		let id = 0;

		for(let x = 0; x < w; x++) {
			let values = [];
			for(let y = 0; y < h; y++) {
				let offset = (y * w + x) * 4;
				let sum = data.slice(offset, offset + 3).reduce((a, b) => a + b, 0);
				if(sum > 30) {
					values.push(1);
				} else {
					values.push(0);
				}
			}

			// To binary string
			let num = parseInt(values.reverse().join(""), 2);
			charData += num + "!";

			if(num === 0) {
				if(optimize && allCharData.indexOf(charData) > -1) {
					// Repeats?
					allCharData.push("");
					repeat.push([allCharData.indexOf(charData), id++]);
					charData = "";
					repeatCnt++;
					continue;
				}

				// Remove previous 0-width word
				if(offsets.slice(-1)[0] === res.length - 1) {
					offsets.pop();
				}

				// Insert
				charData.split("!").forEach(a => {
					if(a !== "") {
						res.push(+a);
					}
				});

				// Next word
				for(let i = 0; i < repeatCnt + 1; i++) {
					offsets.push(res.length);
				}
				allCharData.push(charData);
				charData = "";
				repeatCnt = 0;
				id++;
			}
		}

		// First 0
		offsets.unshift(0);

		let resText = [];
		// Char count
		resText.push(offsets.length);
		// Offsets
		offsets.forEach(offset => resText.push(offset * (h == 8 ? 1 : 2) + (offsets.length + 1) * 2));

		// Data
		if(h == 8) {
			let _16bit = [];
			for(let i = 0; i < res.length; i += 2) {
				let value = res[i] | (res[i + 1] << 8);
				_16bit.push(value);
			}
			if(res.length % 2) {
				_16bit.push(res[res.length - 1]);
			}

			resText = resText.concat(_16bit);
		} else {
			resText = resText.concat(res);
		}

		// .bin
		if(from != null) {
			resText = [from, resText.length].concat(resText);
		}

		// To bytes
		resText = resText.map(value => [value & 0xFF, value >> 8]).reduce((a, b) => a.concat(b), []);
		resText = new Uint8Array(resText);

		let blob = new Blob([resText]);
		let url = URL.createObjectURL(blob);

		let a = document.createElement("a");
		a.href = url;
		const name = file.name.replace(/\..*?$/, "");
		a.download = name + (from == null ? ".raw" : ".bin");
		document.body.appendChild(a);
		a.click();

		ident.innerHTML = "";
		repeat.forEach(a => {
			const tr = document.createElement("tr");

			const td1 = document.createElement("td");
			td1.textContent = koi8rToUtf8(a[1]) + " (" + toId(a[1]) + ")";
			tr.appendChild(td1);

			const td2 = document.createElement("td");
			td2.textContent = koi8rToUtf8(a[0]) + " (" + toId(a[0]) + ")";
			tr.appendChild(td2);

			ident.appendChild(tr);
		});
	});
}

function toCanvas(file, cb) {
	toUri(file, uri => {
		const img = new Image();
		img.onload = () => {
			const canvas = document.createElement("canvas");
			canvas.width = img.width;
			canvas.height = img.height;

			const ctx = canvas.getContext("2d");
			ctx.fillStyle = "#000";
			ctx.fillRect(0, 0, img.width, img.height);
			ctx.drawImage(img, 0, 0);

			cb(img.width, img.height, ctx);
		};
		img.src = uri;
	});
}

function toUri(file, cb) {
	const fr = new FileReader();
	fr.onload = () => {
		cb(fr.result);
	};
	fr.readAsDataURL(file);
}



function koi8rToUtf8(code) {
	const koi2utf = {
		163: 1105,
		179: 1025,
		192: 1102,
		193: 1072,
		194: 1073,
		195: 1094,
		196: 1076,
		197: 1077,
		198: 1092,
		199: 1075,
		200: 1093,
		201: 1080,
		202: 1081,
		203: 1082,
		204: 1083,
		205: 1084,
		206: 1085,
		207: 1086,
		208: 1087,
		209: 1103,
		210: 1088,
		211: 1089,
		212: 1090,
		213: 1091,
		214: 1078,
		215: 1074,
		216: 1100,
		217: 1099,
		218: 1079,
		219: 1096,
		220: 1101,
		221: 1097,
		222: 1095,
		223: 1098
	};

	code = toId(code);

	if(koi2utf[code] != null) {
		code = koi2utf[code];
	} else if(code > 223 && koi2utf[code - 32] != null){
		code = koi2utf[code - 32] - 32;
	}
	return String.fromCharCode(code);
}

function toId(code) {
	code += 33;
	if(code >= 128) {
		code += 64;
	}
	return code;
}
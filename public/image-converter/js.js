function convert(file, from, color) {
	toUri(file, uri => {
		toCanvas(uri, canvas => {
			const ctx = canvas.getContext("2d");
			const data = ctx.getImageData(0, 0, canvas.width, canvas.height).data;

			let bytes = [];
			let byte = 0;
			let byteLength = 0;
			for(let i = 0; i < data.length; i += 4) {
				const [r, g, b, a] = data.slice(i, i + 4);

				let bits;
				if(color) {
					if(r > 100 || g > 100 || b > 100) {
						bits = 0b1;
					} else {
						bits = 0b0;
					}
					byte |= bits << byteLength;
					byteLength++;
				} else {
					if(r > 100) {
						bits = 0b11;
					} else if(g > 100) {
						bits = 0b10;
					} else if(b > 100) {
						bits = 0b01;
					} else {
						bits = 0b00;
					}
					byte |= bits << byteLength;
					byteLength += 2;
				}

				if(byteLength === 8) {
					bytes.push(byte);
					byte = 0;
					byteLength = 0;
				}
			}

			if(byteLength !== 0) {
				bytes.push(byte);
			}

			if(from !== null) {
				bytes = [from & 0xFF, from >> 8, bytes.length & 0xFF, bytes.length >> 8].concat(bytes);
			}

			let blob = new Blob([new Uint8Array(bytes)], {type: "image/bk0010-raw"})

			const url = URL.createObjectURL(blob);

			let a = document.createElement("a");
			a.href = url;
			const name = file.name.replace(/\..*?$/, "");
			a.download = name + (from ? ".bin" : ".raw");
			document.body.appendChild(a);
			a.click();
			document.body.removeChild(a);
		});
	});
}

function toUri(file, cb) {
	const fr = new FileReader();
	fr.onload = () => {
		cb(fr.result);
	};
	fr.readAsDataURL(file);
}

function toCanvas(uri, cb) {
	const img = new Image();
	img.onload = () => {
		let canvas = document.createElement("canvas");
		canvas.width = img.width;
		canvas.height = img.height;

		let ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0);

		cb(canvas);
	};
	img.src = uri;
}